<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                name: {
                    required: true,
                    minlength: 10
                },
                address: {
                    required: true,
                    minlength: 3
                },
                contact: {
                    required: true,
                    minlength: 7,
                    maxlength: 10,
                    digits: true
                },
                email: {
                    required: true,
                    minlength: 7,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 7
                },





            },
            messages: {
                name: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 10 characters long"
                },
                address: {
                    required: "Please input an address.",
                    minlength: "Your Address must be at least 3 characters long",
                },
                contact: {
                    required: "Please input a contact.",
                    minlength: "Your Contact must be at least 7 characters long",
                    maxlength: "only 10 characters allowed",
                    digits: "only numbers allowed",
                },
                email: {
                    required: "Please input a Email Address",
                    minlength: "Your Email must be at least 7 characters long",
                    email: "Please input valid email",
                },
                password: {
                    required: "Please input a Password",
                    minlength: "Your password must be at least 7 characters long",
                }
            },

        });

    });
</script>
