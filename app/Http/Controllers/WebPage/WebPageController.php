<?php

namespace App\Http\Controllers\WebPage;

use App\Models\AboutUs;
use App\Models\Banner;
use App\Models\Feature;
use App\Models\Feedback;
use App\Models\Gallery;
use App\Models\News;
use App\Models\Page;
use App\Models\Photo;
use App\Models\Principle;
use App\Models\Profile;
use Illuminate\Http\Request;

class WebPageController extends ComposerController
{
    public function index(){

        $data = [];
        $data['banner'] = Banner::select()->where('title','banner')->first();
        $data['principle'] = Principle::select()->first();
        $data['profile'] = Profile::select()->first();
        $data['feature'] = Feature::select()->get();
        $data['about'] = News::select()->get();
        $data['gallery'] = Gallery::select()->get();
        $data['feedback'] = Feedback::select()->get();

        return view(parent::commonfunction('frontend.home.index'), compact('data'));

    }

    public function news($slug=null)
    {
        if (isset($slug)){

            $data['news'] = News::select()->where('slug','=',$slug)->first();
            return view(parent::commonfunction('frontend.home.newspage'), compact('data'));
        }

            else{

                $data['news'] = News::select()->get();
                return view(parent::commonfunction('frontend.home.newspage'), compact('data'));
            }
    }

    public function feedback(Request $request)
    {
dd($request->all());

        $file_name = '';

        if ($request->hasFile('image')){

            $image = $request->file('image');
            $file_name = rand(0000,9999). '_' .$image->getClientOriginalName();
            $destinationPath = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'feedback'.DIRECTORY_SEPARATOR;

            $image->move($destinationPath,$file_name);

        }

        $a = Feedback::insert([
            'name' => $request->get('name'),
            'email' => 'a@b.c',
            'role' => $request->get('role'),
            'image' => $file_name,
            'message' => $request->get('message'),
            'status' => 0
        ]);

        $request->session()->flash('message', 'feedback request sent...');
        return redirect()->route('index');

    }

    public function page($slug)
    {
        $data['page'] = Page::select()->where('slug','=',$slug)->first();
        return view(Parent::commonfunction('frontend.home.page'), compact('data'));
    }

    public function gallery($slug=null)
    {

        if (isset($slug)){

            $data['gallery'] = Photo::select()->where('gallery_id','=',$slug)->get();
            $data['photo'] = $slug;

        }else{

            $data['gallery'] = Gallery::select()->get();

        }

        return view(Parent::commonfunction('frontend.home.gallerypage'), compact('data'));
    }

    public function alumni()
    {
        dd('alumni');
    }

    public function careers()
    {
        dd('careers');
    }

    public function contact()
    {
        $data = [];

        $data['banner'] = Banner::select('image')->where('title','=','contact')->first();

        return view(Parent::commonfunction('frontend.home.contact'), compact('data'));
    }

    public function download()
    {
        dd('download');
    }

    public function about()
    {
        $data['page'] = [
            'title' => 'About Us',
            'description' => 'No About us'
        ];
        return view(Parent::commonfunction('frontend.home.page'), compact('data'));
    }

    public function information()
    {
        $data['page'] = [
            'title' => 'Information',
            'description' => 'No Information'
        ];
        return view(Parent::commonfunction('frontend.home.page'), compact('data'));
    }

    public function clubAndEca()
    {
        $data['page'] = [
            'title' => 'Club and ECA',
            'description' => 'No Club and ECA'
        ];
        return view(Parent::commonfunction('frontend.home.page'), compact('data'));
    }
    
}
