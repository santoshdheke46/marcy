@extends('frontend.common.layout')

@section('content')

    <style>
        .pages{
            min-height: 720px;
            margin-top: 88px;
        }
        .contact {
            background: url({{ asset('images/banner/'.$data['banner']->image) }})no-repeat;
            background-size: cover;
            min-height:500px;
            padding: 5em 0em;
        }
    </style>

    @include('frontend.common.header')

    <div class="pages" id="portfolio">

        <!--contact start here-->
        <div class="contact" id="contact">
            <div class="conhaf">
                <div class="container">
                    <div class="contact-main">
                        <div class="contact-top wow fadeInDown" data-wow-delay="0.3s">
                            <h3>Say Hello</h3>
                            <span class="heading-line"> </span>
                            <p>Nemo enim ipsam voluptatem quia.</p>
                        </div>
                        <div class="contact-bottom">
                            <div class="col-md-6 contact-left wow fadeInLeft" data-wow-delay="0.3s">
                                <form action="{{ route('feedback.store') }}" method="get">
                                    {{ csrf_field() }}
                                    <input type="text" name="name" placeholder="Name">
                                    <input type="text" name="email" placeholder="Email">
                                    <textarea name="message"  placeholder="Message"></textarea>
                                    <input type="submit" name="btn" value="Send Message">
                                </form>
                            </div>
                            <div class="col-md-6 contact-right wow fadeInRight" data-wow-delay="0.3s">
                                <h4>Contact Info</h4>
                                <p> Nemo enim ipsam voluptatem</p>
                                <p>These cases are perfectly </p>
                                <ul>
                                    <li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"> </span>Professor at Hampden-Sydney</li>
                                    <li><span class="glyphicon glyphicon-phone" aria-hidden="true"> </span>+1284 485 978</li>
                                    <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"> </span><a href="mailto:info@example.com">@example.com</a></li>
                                </ul>

                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--contact end here-->
        <!--map start here-->
        <div class="map">
            <div id="g-map">
                <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key= AIzaSyBD7PzmUXIRWBx29SsfouCR25MZ8lHJFJs '></script>
                <div style='overflow:hidden;/*height:390px;width:620px;*/'>
                    <div id='gmap_canvas' style='height:400px;width: 100%;'></div>
                    <style type="text/css">#gmap_canvas img{max-width:none!important;background:none!important}</style>
                </div>
                <script type='text/javascript'>
                    function init_map() {
                        var myOptions = {
                            zoom:13,
                            center:new google.maps.LatLng(27.697122,85.362375),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                        marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(27.697122,85.362375)});

                        infowindow = new google.maps.InfoWindow({content:'mercy school'});
                        google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});
                        infowindow.open(map,marker);
                    }
                    google.maps.event.addDomListener(window, 'load', init_map);
                </script>
            </div>
        </div>
        <!--//map-->

    </div>

    @include('frontend.common.footer')

@endsection