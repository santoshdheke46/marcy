<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            'title' => 'Home',
            'slug' => 'home',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 1,
            'target' => 0,
            'order_by' => 1,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'About Us',
            'slug' => 'about-us',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 2,
            'target' => 0,
            'order_by' => 2,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Information',
            'slug' => 'information',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 3,
            'target' => 0,
            'order_by' => 3,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Club and Eca',
            'slug' => 'club-and-eca',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 4,
            'target' => 0,
            'order_by' => 4,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Download',
            'slug' => 'download',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 5,
            'target' => 0,
            'order_by' => 5,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Careers',
            'slug' => 'careers',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 6,
            'target' => 0,
            'order_by' => 6,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Alumni',
            'slug' => 'alumni',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 7,
            'target' => 0,
            'order_by' => 6.5,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Gallery',
            'slug' => 'gallery',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 8,
            'target' => 0,
            'order_by' => 7,
            'status' => 1,
        ]);
        DB::table('menu')->insert([
            'title' => 'Contact',
            'slug' => 'contact',
            'link' => null,
            'parent_menu' => 0,
            'page_id' => 9,
            'target' => 0,
            'order_by' => 8,
            'status' => 1,
        ]);
    }
}
