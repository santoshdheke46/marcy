<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Profile\EditValidation;
use App\Models\Profile;
use Illuminate\Http\Request;


class ProfileController extends AdminBaseController
{
    protected $base_route = 'admin.profile';
    protected $view_path = 'admin.profile';
    protected $view_title = 'Profile Manger';
    protected $folder_name = 'profile';
    protected $trans_path = 'profile';
    protected $listing;
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function edit(Request $request)
    {
        // get user data for $id
        //dd($id);
        $data = [];
        $data['row'] = Profile::select()->first();

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {



        if (!$listing = Profile::find($id)){

            if ($request->hasFile('image')){
                $image = $request->file('image');
                $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
                $image->move($this->folder_path,$file_name);
            }else{
                $file_name = $request->get('oldimg');
            }

            $lii=Profile::create([
                'title' => $request->get('title'),
                'image' => $file_name,
                'country' => $request->get('country'),
                'city' => $request->get('city'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'email' => $request->get('email'),
                'facebook' => $request->get('facebook'),
                'twitter' => $request->get('twitter'),
                'google' => $request->get('google'),
                'skype' => $request->get('skype'),
                'linkedin' => $request->get('linkedin'),
            ]);
        }else{

            if ($request->hasFile('image')){
                $image = $request->file('image');
                $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
                $image->move($this->folder_path,$file_name);
//                if(!empty($listing->image) && isset($listing->image))
//                    unlink($this->folder_path.$listing->image);
            }else{
                $file_name = $request->get('oldimg');
            }

            $listing->update([
                'title' => $request->get('title'),
                'image' => $file_name,
                'country' => $request->get('country'),
                'city' => $request->get('city'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'email' => $request->get('email'),
                'facebook' => $request->get('facebook'),
                'twitter' => $request->get('twitter'),
                'google' => $request->get('google'),
                'skype' => $request->get('skype'),
                'linkedin' => $request->get('linkedin'),
            ]);
        }






        $request->session()->flash('message', 'Profile updated successfully.');
        return redirect()->route($this->base_route);
    }

    public function delete(Request $request, $id)
    {

        $responce = [];
        $responce['error'] = true;

        if (!$listing = Profile::find($id))
            $responce['message'] = "wrong";

        $listing->update([
            'image' => null
        ]);

        $responce['error'] = false;

        return response()->json(json_encode($responce));

    }



}