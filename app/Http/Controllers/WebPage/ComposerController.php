<?php

namespace App\Http\Controllers\WebPage;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Feature;
use App\Models\News;
use App\Models\Principle;
use App\Models\Profile;
use App\Models\Menu;
use View;

class ComposerController extends Controller
{

    public function commonfunction($path)
    {

        View::composer($path,function ($view){

            $view->with('profile', Profile::first());
            $view->with('principal', Principle::first());
            $view->with('feature', Feature::select('title', 'slug', 'description','status')->where('status', 1)->get());
            /*$about_us_id = Menu::select('id')->where('slug','=','about-us')->first();
            $view->with('about', Menu::select()->where('parent_menu','=',$about_us_id->id)->get());
            $information_id = Menu::select('id')->where('slug','=','information')->first();
            $view->with('information', Menu::select()->where('parent_menu','=',$information_id->id)->get());
            $club_and_eca_id = Menu::select('id')->where('slug','=','club-and-eca')->first();
            $view->with('club_and_eca', Menu::select()->where('parent_menu','=',$club_and_eca_id->id)->get());*/
            $view->with('menu', Menu::select()->get());

        });

        return $path;
        
    }

}
