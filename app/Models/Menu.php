<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = ['title', 'slug', 'link', 'parent_menu', 'page_id', 'order_by', 'target', 'status'];

    public function parent()
    {
        return $this->belongsTo('App\Models\Menu', 'parent_menu');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_menu');
    }

    public function page()
    {
        return $this->belongsTo('App\Models\Page','page_id');
    }


}
