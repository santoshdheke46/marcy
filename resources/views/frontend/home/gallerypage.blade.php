@extends('frontend.common.layout')

@section('content')

    <style>
        .pages{
            min-height: 600px;
            margin-top: 120px;
        }
    </style>

    @include('frontend.common.header')

    <div class="pages" id="portfolio">

        <div class="portfolio" id="portfolio">
            <div class="container">
                <div class="portfolio-top wow fadeInDown" data-wow-delay="0.3s">
                    <h3>{{ isset($data['photo'])?$data['photo']:'Gallery' }}</h3>
                    <span class="heading-line"> </span>
                    <p>Nemo enim ipsam voluptatem quia.</p>
                </div>
                <div class="sap_tabs">

                    <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                        @if(isset($data['gallery']) && count($data['gallery'])>0)

                            <div class="resp-tabs-container">
                                <div class="" aria-labelledby="tab_item-0">
                                    <div class="tab_img">
                                        @php($counter = 0)
                                        @foreach($data['gallery'] as $gallery)
                                            @php($counter++)
                                            {{--@for ($cf=0;$cf<$counter;$cf=$cf+4)
                                            {{ $cf }}
                                                @endfor--}}
                                            <div class="col-md-3 img-top">
                                            <div class="gal-one">
                                                <a href="{{ isset($data['photo'])?'#'.$data['photo'].$counter:route('photo',$gallery->slug) }}">
                                                    <figure class="effect-apollo">
                                                        <img src="{{ asset('images/gallery/'.$gallery->image) }}" alt="image1" style="height: 200px;width: 100%;" class="img-responsive">
                                                        <div class="link-top">
                                                            <h1 style="color: #ffffff;border-bottom: 1px #ffffff solid;border-top: 1px #ffffff solid;">{{ $gallery->title }}</h1>
                                                        </div>
                                                    </figure>
                                                </a>
                                                <div class="lb-overlay" id="{{ isset($data['photo'])?$data['photo'].$counter:route('photo',$gallery->slug) }}">
                                                    <img src="{{ asset('images/gallery/'.$gallery->image) }}" alt="image1" class="img-responsive">
                                                    <div class="gal-info">
                                                        <h3>{{ $gallery->title }}</h3>
                                                        <p>{!! $gallery->description !!}</p>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                    <a href="#" class="lb-close">Close</a>
                                                </div>
                                            </div>
                                    </div>
                                    @if($counter%4==0)
                                        <div class="clearfix"> </div>
                                </div>
                                <div class="tab_img">
                                    @endif
                                    @endforeach
                                    <div class="clearfix"> </div>
                                </div>

                            </div>
                    </div>

                    @else

                        <h1>No {{ isset($data['photo'])?$data['photo']:'Gallery' }} !!!</h1>

                    @endif
                </div>
            </div>
        </div>
    </div>

    </div>

    @include('frontend.common.footer')

@endsection