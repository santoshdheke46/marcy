@extends('frontend.common.layout')

@section('content')

    <style>
        .pages{
            min-height: 600px;
            margin-top: 120px;
        }
    </style>

    @include('frontend.common.header')

    <div class="pages" id="portfolio">
        <div class="container">
            <h3 class="top-head w_hd">{{ $data['page']['title'] }}</h3>
            <span class="line w_l_1">
				<span class="sub-line w_l_1"></span>
			</span>
            <div style="min-height: 400px;">
                <p>{!! $data['page']['description'] !!}</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    @include('frontend.common.footer')

@endsection