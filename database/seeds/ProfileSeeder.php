<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([
            'title' => 'Schoolname',
            'image' => 'logo',
            'country' => 'nepal',
            'city' => 'kathmandu',
            'phone' => 1111111111,
            'mobile' => 1111111111,
            'email' => 'a@b.c',
            'facebook' => null,
            'twitter' => null,
            'google' => null,
            'skype' => null,
            'linkedin' => null,
        ]);
    }
}
