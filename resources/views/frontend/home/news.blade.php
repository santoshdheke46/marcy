<div class="service" id="services">
    <div class="container">
        <div class="service-main">
            <div class="service-top wow fadeInDown" data-wow-delay="0.3s">
                <h3>News</h3>
                <span class="heading-line"> </span>
                <p>Nemo enim ipsam voluptatem quia.</p>
            </div>
            <div class="services-bottom">
                <div class="serice-layer wow fadeInRight" data-wow-delay="0.3s">
                    @if(isset($data['news']))
                        @if(count($data['news'])==1)
                            <div class="col-md-12 services-grid">
                                <div class="col-md-3 serv-img">
                                    <a href="#"><img src="{{ asset('images/news/'.$data['news']->image) }}" alt=""
                                                     class="img-responsive"></a>
                                    <div class="blog-discription">
                                        <div class="theme-border">
                                            <div class="tg-display-table">
                                                <div class="tg-display-table-cell">
                                                    <div class="blog-title">
                                                        <h4><a href="#">Detail</a></h4>
                                                        <ul class="blod-meta">
                                                            <li>Dated: {{ $data['news']->date }}</li>
                                                            <div class="clearfix"></div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 serv-text">
                                    <h4>{{ $data['news']->title }}</h4>
                                    <p>{{ $data['news']->description }}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @else
                            @if(isset($data['news']))
                                @php($counter=0)
                                @foreach($data['news'] as $news)
                                    @php($counter++)
                                    <div class="col-md-6 services-grid">
                                        <div class="col-md-6 serv-img">
                                            <a href="#"><img src="{{ asset('images/news/'.$news->image) }}" alt=""
                                                             class="img-responsive"></a>
                                            <div class="blog-discription">
                                                <div class="theme-border">
                                                    <div class="tg-display-table">
                                                        <div class="tg-display-table-cell">
                                                            <div class="blog-title">
                                                                <h4><a href="{{ route('newss',$news->slug) }}">Detail</a></h4>
                                                                <ul class="blod-meta">
                                                                    <li>Dated: {{ $news->date }}</li>
                                                                    <div class="clearfix"></div>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 serv-text">
                                            <h4>{{ $news->title }}</h4>
                                            <p>{{ $news->description }}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if($counter==2)
                                        <div class="clearfix"></div>
                </div>
                <div class="serice-layer wow fadeInLeft" data-wow-delay="0.3s">
                    @endif
                    @endforeach
                    @endif
                    @endif
                    @else
                        <h1>no data found</h1>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>