<?php

namespace App\Http\Controllers\WebPage;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Feature;
use App\Models\News;
use App\Models\Principle;
use App\Models\Profile;
use App\Models\Menu;
use View;

class ComposerController extends Controller
{

    public function commonfunction($path)
    {

        View::composer($path,function ($view){

            $view->with('profile', Profile::first());
            $view->with('feature', Feature::select('title', 'slug', 'description','status')->where('status', 1)->get());
            $view->with('about', AboutUs::select('title', "description", 'status')->where('slug', 'main')->first());
            $view->with('menu', Menu::select()->get());

        });

        return $path;
        
    }

}
