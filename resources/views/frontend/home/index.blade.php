@extends('frontend.common.layout')

@section('content')

    <!--banner start here-->

    @include('frontend.home.banner')

    <!--banner start here-->
    <!--header start here-->
    <!-- NAVBAR
        ================================================== -->

    @include('frontend.common.header')

    <!--header end here-->
    <!--about start here-->

    @include('frontend.home.newss')

    <!--about end here-->
    <!--team start here-->

    @include('frontend.home.principale')

    <!--team end here-->
    <!--services start here-->

    @include('frontend.home.gallery')

    <!--gallery-->
    <script src="{{ asset('frontend/js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });

    </script>
    <!--advantages start here-->

    @include('frontend.home.feedback')

    <!--advantages end here-->

    <!--copy rights start here-->

    @include('frontend.common.footer')

    <!--copy rights end here-->

    @endsection