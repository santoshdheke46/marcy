<div class="portfolio" id="portfolio">
    <div class="container">
        <div class="portfolio-top wow fadeInDown" data-wow-delay="0.3s" style="padding-bottom: 0px;">
            <h3>Portfolio</h3>
            <span class="heading-line"> </span>
            <p>Nemo enim ipsam voluptatem quia.</p>
        </div>
        <div class="sap_tabs">

            <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                <ul class="resp-tabs-list">
                    <li aria-controls="tab_item-0" role="tab"></li>
                    <div class="clearfix"> </div>
                </ul>
                <div class="resp-tabs-container">
                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                        <div class="tab_img">
                            @php($counter = 0)
                            @foreach($data['gallery'] as $gallery)
                                @php($counter++)
                            {{--<div class="col-md-3 img-top ">
                                <div class="gal-one">
                                    <a href="#image-1">
                                        <figure class="effect-apollo">
                                            <img src="{{ asset('frontend/images/p1.jpg') }}" alt="image1"  class="img-responsive">
                                            <div class="link-top">
                                                <i class="link"> </i>
                                            </div>
                                        </figure>
                                    </a>
                                    <div class="lb-overlay" id="image-1">
                                        <img src="{{ asset('frontend/images/p1.jpg') }}" alt="image1" class="img-responsive">
                                        <div class="gal-info">
                                            <h3>Coaching</h3>
                                            <p>Neque porro quisquam est, qui dolorem ipsum
                                                quia dolor sit amet, consectetur, adipisci velit,
                                                sed quia non numquam.</p>
                                            <div class="clearfix"> </div>
                                        </div>
                                        <a href="index.html" class="lb-close">Close</a>
                                    </div>
                                </div>
                            </div>--}}
                                <div class="col-md-3 img-top">
                                <div class="gal-one">
                                    <a href="{{ isset($data['photo'])?'#'.$data['photo'].$counter:route('photo',$gallery->slug) }}">
                                        <figure class="effect-apollo">
                                            <img src="{{ asset('images/gallery/'.$gallery->image) }}" alt="image1" style="height: 200px;width: 100%;" class="img-responsive">
                                            <div class="link-top">
                                                <h1 style="color: #ffffff;border-bottom: 1px #ffffff solid;border-top: 1px #ffffff solid;">{{ $gallery->title }}</h1>
                                            </div>
                                        </figure>
                                    </a>
                                    <div class="lb-overlay" id="{{ isset($data['photo'])?$data['photo'].$counter:route('photo',$gallery->slug) }}">
                                        <img src="{{ asset('images/gallery/'.$gallery->image) }}" alt="image1" class="img-responsive">
                                        <div class="gal-info">
                                            <h3>{{ $gallery->title }}</h3>
                                            <p>{!! $gallery->description !!}</p>
                                            <div class="clearfix"> </div>
                                        </div>
                                        <a href="#" class="lb-close">Close</a>
                                    </div>
                                </div>
                                </div>
                                @if($counter%4==0)
                                    <div class="clearfix"> </div>
                        </div>
                        <div class="tab_img">
                            @endif
                            @if($counter>8)
                                @break
                                @endif
                            @endforeach
                            <div class="clearfix"> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>