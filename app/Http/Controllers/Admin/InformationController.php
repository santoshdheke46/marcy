<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\AboutUs\AddValidation;
use App\Http\Requests\AboutUs\EditValidation;
use App\Models\AboutUs;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;


class InformationController extends AdminBaseController
{
    protected $base_route = 'admin.information';
    protected $view_path = 'admin.information';
    protected $view_title = 'Information Manger';
    protected $folder_name = 'information';
    public $trans_path = 'santosh';
    protected $listing;
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        $menu_id = Menu::select('id')->where('slug','=',$this->folder_name)->first();
        $data = [];
        $data['rows'] = Menu::select()->where('parent_menu','=',$menu_id->id)->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

        $slug = str_slug($request->get('title'));

        Page::create([
            'title' => $request->get('title'),
            'slug' => $slug,
            'description' => $request->get('description'),
            'parent_menu' => $request->get('parent_menu'),
            'page_link' => $request->get('page_link'),
            'status' => $request->get('status'),
        ]);

        $parent_menu = Menu::select('id')->where('slug','=',$this->folder_name)->first();
        $page_id = Page::select('id')->where('slug','=',$slug)->first();

        Menu::create([
            'title' => $request->get('title'),
            'slug' => $slug,
            'link' => null,
            'page_id' => $page_id->id,
            'parent_menu' => $parent_menu->id,
            'target' => 0,
            'status' => $request->get('status'),
        ]);

        /*if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }

       $list = AboutUs::create([
           'title'               =>     $request->get('title'),
           'slug' => str_slug($request->get('title')),
           'image'               =>     $file_name,
           'description'               =>     $request->get('description'),
           'status'          =>     $request->get('status'),
        ]);*/

        $request->session()->flash('message', 'AboutUs added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $menu_id, $page_id)
    {

        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['menu'] = Menu::find($menu_id))
            return redirect()->route('admin.error', ['code' => '500']);

        if (!$data['santosh'] = Page::find($page_id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data['menu_id'] = $data['menu']->id;
        $data['page_id'] = $data['santosh']->id;

        $data['row'] = Menu::select()->first();

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $menu_id, $page_id)
    {
        if (!$menu = Menu::find($menu_id))
            return redirect()->route('admin.error', ['code' => '500']);

        if (!$page = Page::find($page_id))
            return redirect()->route('admin.error', ['code' => '500']);

        $page->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'parent_menu' => $request->get('parent_menu'),
            'page_link' => $request->get('page_link'),
            'status' => $request->get('status'),
        ]);

        $menu->update([
            'title' => $request->get('title'),
            'status' => $request->get('status'),
        ]);


        $request->session()->flash('message', 'AboutUs updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $menu_id, $page_id)
    {
       //dd('enter');
        if (!$menu = Menu::find($menu_id))
            return redirect()->route('admin.error', ['code' => '500']);

        if (!$page = Page::find($page_id))
            return redirect()->route('admin.error', ['code' => '500']);

        $menu->delete();
        $page->delete();
        $request->session()->flash('message', 'AboutUs deleted successfully.');
        return redirect()->route($this->base_route.'.index');

    }

    public function imgDelete(Request $request)
    {

        $id = $request->get('id');

        $response = [];
        $response['error'] = true;

        if (!$listing = AboutUs::find($id)){
            $response['message'] = 'data not found';
        }else{

            if($listing->image ==! '')
            $listing->update([
                'image'               =>     null,
            ]);

            $response['error'] = false;

        }

        return response()->json(json_encode($response));

    }

    public function search(Request $request)
    {
        dd($request->all());
    }

}