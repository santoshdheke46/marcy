<div class="header">
    <div class="fixed-header">

        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-static-top">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="logo wow slideInLeft" data-wow-delay="0.3s">
                            <a class="navbar-brand" href="{{ route('index') }}" style="width: 100%"><img src="{{ asset('images/profile/'.$profile->image) }}" style="float: left;height: 30px;"/>
                                <h1 style="color: #1a1a1a;">{{ $profile->title }}</h1>
                            </a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <nav class="cl-effect-1">
                            @if(isset($menu) && count($menu)>0)
                                <ul class="nav navbar-nav">
                                    @foreach($menu as $m)
                                        @if($m->parent_menu!=0)
                                            @continue
                                            @endif
                                        <li class="{{ count($m->children)>0?'parent':'' }}">
                                            <a href="{{ count($m->children)>0?'#':route($m->page->page_link) }}" class="{{ count($m->children)>0?'btnu':'' }}">{{ $m->title }}</a>
                                            @if(count($m->children)>0)
                                                <ul class="navbar-nav child">
                                                    @foreach($m->children as $c)
                                                        <li class="floatnon"><a href="{{ route($c->page->page_link ,$c->slug) }}">{{ $c->title }}</a></li>
                                                        @endforeach
                                                </ul>
                                                @endif
                                        </li>
                                        @endforeach
                                </ul>
                                @endif
                        </nav>
                        <!-- script-for sticky-nav -->
                        <script>
                            $(document).ready(function() {
                                var navoffeset=$(".header").offset().top;
                                $(window).scroll(function(){
                                    var scrollpos=$(window).scrollTop();
                                    if(scrollpos >=navoffeset){
                                        $(".header").addClass("fixed");
                                    }else{
                                        $(".header").removeClass("fixed");
                                    }
                                });

                            });
                        </script>
                        <!-- /script-for sticky-nav -->

                    </div>
                    <div class="clearfix"> </div>
                </nav>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>