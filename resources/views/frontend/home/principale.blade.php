<style>
    .team{
        background-color: #0000cc;
    }
</style>

<div class="team">
    <div class="container">
        <div class="team-main">
            <div class="team-top wow fadeInDown" data-wow-delay="0.3s">
                <h3>Principale Message</h3>
                <span class="heading-line"> </span>
                <p>Nemo enim ipsam voluptatem quia.</p>
            </div>
            <div class="team-bottom wow fadeInRight" data-wow-delay="0.3s">
                <div class="col-md-4 team-grids">
                    <img src="{{ asset('images/principle/'.$data['principle']->image) }}" alt="img" class="img-responsive">
                </div>
                <div class="col-md-8 team-grids">
                    <div class="team-bottom">
                        <p>{{ $data['principle']->message }}</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>