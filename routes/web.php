<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('',                    ['as' => 'index',             'uses' => 'WebPage\WebPageController@index']);
Route::get('about',                    ['as' => 'about',             'uses' => 'WebPage\WebPageController@about']);
Route::get('gallerys',                    ['as' => 'gallery',             'uses' => 'WebPage\WebPageController@gallery']);
Route::get('gallerys/{slug}',                    ['as' => 'photo',             'uses' => 'WebPage\WebPageController@gallery']);
Route::get('contact-us',                    ['as' => 'contact-us',             'uses' => 'WebPage\WebPageController@contact']);
Route::get('newses/{slug}',                    ['as' => 'newss',             'uses' => 'WebPage\WebPageController@news']);
Route::get('newses',                    ['as' => 'newses',             'uses' => 'WebPage\WebPageController@news']);
Route::get('alumni',                    ['as' => 'alumni',             'uses' => 'WebPage\WebPageController@alumni']);
Route::get('careers',                    ['as' => 'careers',             'uses' => 'WebPage\WebPageController@careers']);
Route::get('informations',                    ['as' => 'informations',             'uses' => 'WebPage\WebPageController@information']);
Route::get('club-and-ecas',                    ['as' => 'club-and-eca',             'uses' => 'WebPage\WebPageController@clubAndEca']);
Route::get('downloads',                    ['as' => 'download',             'uses' => 'WebPage\WebPageController@download']);
Route::get('simplepage/{slug}',                    ['as' => 'simplepage',             'uses' => 'WebPage\WebPageController@page']);
Route::post('feedback/stores',                 ['as' => 'feedback.store',           'uses' => 'Admin\FeedbackController@store']);

Auth::routes();

//Route::get('/', function () {
//    return redirect('login');
//});

Route::get('/home', function () {
    return redirect()->route('admin.dashboard');
});

Route::group(['middleware' => ['auth', 'status'], 'prefix' => '', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('error/{code}',                        ['as' => 'error',                 'uses' => 'DashboardController@error']);

    Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@edit']);
    Route::post('profile/{id}/update', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::post('profile/{id}/delete', ['as' => 'profile.delete', 'uses' => 'ProfileController@delete']);

    Route::get('menu',['as' => 'menu', 'uses' => 'MenuController@index']);

    Route::get('principle',                    ['as' => 'principle.add',             'uses' => 'PrincipleController@add']);
    Route::post('principle/{id}/update',          ['as' => 'principle.update',          'uses' => 'PrincipleController@update']);
    Route::post('principle/{id}/delete',          ['as' => 'principle.image.delete',          'uses' => 'PrincipleController@imageDelete']);

    Route::get('about-us',                        ['as' => 'about-us.index',           'uses' => 'AboutUsController@index']);
    Route::post('about-us-search',                        ['as' => 'about-us.search',           'uses' => 'AboutUsController@search']);
    Route::get('about-us/add',                    ['as' => 'about-us.add',             'uses' => 'AboutUsController@add']);
    Route::post('about-us/store',                 ['as' => 'about-us.store',           'uses' => 'AboutUsController@store']);
    Route::get('about-us/{menu_id}/{page_id}/edit',              ['as' => 'about-us.edit',            'uses' => 'AboutUsController@edit']);
    Route::post('about-us/{menu_id}/{page_id}/update',          ['as' => 'about-us.update',          'uses' => 'AboutUsController@update']);
    Route::get('about-us/{menu_id}/{page_id}/delete',           ['as' => 'about-us.delete',          'uses' => 'AboutUsController@delete']);
    Route::post('about-us/delete',           ['as' => 'image.about-us.delete',          'uses' => 'AboutUsController@imgDelete']);

    Route::get('information',                        ['as' => 'information.index',           'uses' => 'InformationController@index']);
    Route::post('information-search',                        ['as' => 'information.search',           'uses' => 'InformationController@search']);
    Route::get('information/add',                    ['as' => 'information.add',             'uses' => 'InformationController@add']);
    Route::post('information/store',                 ['as' => 'information.store',           'uses' => 'InformationController@store']);
    Route::get('information/{menu_id}/{page_id}/edit',              ['as' => 'information.edit',            'uses' => 'InformationController@edit']);
    Route::post('information/{menu_id}/{page_id}/update',          ['as' => 'information.update',          'uses' => 'InformationController@update']);
    Route::get('information/{menu_id}/{page_id}/delete',           ['as' => 'information.delete',          'uses' => 'InformationController@delete']);
    Route::post('information/delete',           ['as' => 'image.information.delete',          'uses' => 'InformationController@imgDelete']);

    Route::get('club-and-eca',                        ['as' => 'club-and-eca.index',           'uses' => 'ClubAndEcaController@index']);
    Route::post('club-and-eca-search',                        ['as' => 'club-and-eca.search',           'uses' => 'ClubAndEcaController@search']);
    Route::get('club-and-eca/add',                    ['as' => 'club-and-eca.add',             'uses' => 'ClubAndEcaController@add']);
    Route::post('club-and-eca/store',                 ['as' => 'club-and-eca.store',           'uses' => 'ClubAndEcaController@store']);
    Route::get('club-and-eca/{menu_id}/{page_id}/edit',              ['as' => 'club-and-eca.edit',            'uses' => 'ClubAndEcaController@edit']);
    Route::post('club-and-eca/{menu_id}/{page_id}/update',          ['as' => 'club-and-eca.update',          'uses' => 'ClubAndEcaController@update']);
    Route::get('club-and-eca/{menu_id}/{page_id}/delete',           ['as' => 'club-and-eca.delete',          'uses' => 'ClubAndEcaController@delete']);
    Route::post('club-and-eca/delete',           ['as' => 'image.club-and-eca.delete',          'uses' => 'ClubAndEcaController@imgDelete']);

    Route::get('gallery',                        ['as' => 'gallery.index',           'uses' => 'GalleryController@index']);
    Route::get('gallery/add',                    ['as' => 'gallery.add',             'uses' => 'GalleryController@add']);
    Route::post('gallery/store',                 ['as' => 'gallery.store',           'uses' => 'GalleryController@store']);
    Route::get('gallery/{id}/edit',              ['as' => 'gallery.edit',            'uses' => 'GalleryController@edit']);
    Route::post('gallery/{id}/update',          ['as' => 'gallery.update',          'uses' => 'GalleryController@update']);
    Route::get('gallery/{id}/delete',           ['as' => 'gallery.delete',          'uses' => 'GalleryController@delete']);

    Route::get('photo',                        ['as' => 'photo.index',           'uses' => 'PhotoController@index']);
    Route::get('photo/add',                    ['as' => 'photo.add',             'uses' => 'PhotoController@add']);
    Route::post('photo/store',                 ['as' => 'photo.store',           'uses' => 'PhotoController@store']);
    Route::get('photo/{id}/edit',              ['as' => 'photo.edit',            'uses' => 'PhotoController@edit']);
    Route::post('photo/{id}/update',          ['as' => 'photo.update',          'uses' => 'PhotoController@update']);
    Route::get('photo/{id}/delete',           ['as' => 'photo.delete',          'uses' => 'PhotoController@delete']);

    Route::get('feature',                        ['as' => 'feature.index',           'uses' => 'FeatureController@index']);
    Route::get('feature/add',                    ['as' => 'feature.add',             'uses' => 'FeatureController@add']);
    Route::post('feature/store',                 ['as' => 'feature.store',           'uses' => 'FeatureController@store']);
    Route::get('feature/{id}/edit',              ['as' => 'feature.edit',            'uses' => 'FeatureController@edit']);
    Route::post('feature/{id}/update',          ['as' => 'feature.update',          'uses' => 'FeatureController@update']);
    Route::get('feature/{id}/delete',           ['as' => 'feature.delete',          'uses' => 'FeatureController@delete']);

    Route::get('feedback',                        ['as' => 'feedback.index',           'uses' => 'FeedbackController@index']);
    Route::get('feedback/add',                    ['as' => 'feedback.add',             'uses' => 'FeedbackController@add']);
    Route::post('feedback/store',                 ['as' => 'feedback.store',           'uses' => 'FeedbackController@store']);
    Route::get('feedback/{id}/edit',              ['as' => 'feedback.edit',            'uses' => 'FeedbackController@edit']);
    Route::post('feedback/{id}/update',          ['as' => 'feedback.update',          'uses' => 'FeedbackController@update']);
    Route::get('feedback/{id}/delete',           ['as' => 'feedback.delete',          'uses' => 'FeedbackController@delete']);

    Route::get('banner',                        ['as' => 'banner.index',           'uses' => 'BannerController@index']);
    Route::get('banner/add',                    ['as' => 'banner.add',             'uses' => 'BannerController@add']);
    Route::post('banner/store',                 ['as' => 'banner.store',           'uses' => 'BannerController@store']);
    Route::get('banner/{id}/edit',              ['as' => 'banner.edit',            'uses' => 'BannerController@edit']);
    Route::post('banner/{id}/update',          ['as' => 'banner.update',          'uses' => 'BannerController@update']);
    Route::get('banner/{id}/delete',           ['as' => 'banner.delete',          'uses' => 'BannerController@delete']);
    Route::post('banner/deleted',           ['as' => 'banner.delete.ajax',          'uses' => 'BannerController@ajaxdelete']);

    Route::get('services',                        ['as' => 'services.index',           'uses' => 'ServicesController@index']);
    Route::get('services/add',                    ['as' => 'services.add',             'uses' => 'ServicesController@add']);
    Route::post('services/store',                 ['as' => 'services.store',           'uses' => 'ServicesController@store']);
    Route::get('services/{id}/edit',              ['as' => 'services.edit',            'uses' => 'ServicesController@edit']);
    Route::post('services/{id}/update',          ['as' => 'services.update',          'uses' => 'ServicesController@update']);
    Route::get('services/{id}/delete',           ['as' => 'services.delete',          'uses' => 'ServicesController@delete']);

    Route::get('news',                        ['as' => 'news.index',           'uses' => 'NewsController@index']);
    Route::get('news/add',                    ['as' => 'news.add',             'uses' => 'NewsController@add']);
    Route::post('news/store',                 ['as' => 'news.store',           'uses' => 'NewsController@store']);
    Route::get('news/{id}/edit',              ['as' => 'news.edit',            'uses' => 'NewsController@edit']);
    Route::post('news/{id}/update',          ['as' => 'news.update',          'uses' => 'NewsController@update']);
    Route::get('news/{id}/delete',           ['as' => 'news.delete',          'uses' => 'NewsController@delete']);

    Route::get('team',                        ['as' => 'team.index',           'uses' => 'teamController@index']);
    Route::get('team/add',                    ['as' => 'team.add',             'uses' => 'teamController@add']);
    Route::post('team/store',                 ['as' => 'team.store',           'uses' => 'teamController@store']);
    Route::get('team/{id}/edit',              ['as' => 'team.edit',            'uses' => 'teamController@edit']);
    Route::post('team/{id}/update',          ['as' => 'team.update',          'uses' => 'teamController@update']);
    Route::get('team/{id}/delete',           ['as' => 'team.delete',          'uses' => 'teamController@delete']);

    Route::get('user',                        ['as' => 'user.index',           'uses' => 'UserController@index']);
    Route::get('user/add',                    ['as' => 'user.add',             'uses' => 'UserController@add']);
    Route::post('user/store',                 ['as' => 'user.store',           'uses' => 'UserController@store']);
    Route::get('user/{id}/edit',              ['as' => 'user.edit',            'uses' => 'UserController@edit']);
    Route::post('user/{id}/update',           ['as' => 'user.update',          'uses' => 'UserController@update']);
    Route::get('user/{id}/delete',            ['as' => 'user.delete',          'uses' => 'UserController@delete']);

    Route::get('menu',                        ['as' => 'menu.index',           'uses' => 'MenuController@index']);
    Route::get('menu/add',                    ['as' => 'menu.add',             'uses' => 'MenuController@add']);
    Route::post('menu/store',                 ['as' => 'menu.store',           'uses' => 'MenuController@store']);
    Route::get('menu/{id}/edit',              ['as' => 'menu.edit',            'uses' => 'MenuController@edit']);
    Route::post('menu/{id}/update',           ['as' => 'menu.update',          'uses' => 'MenuController@update']);
    Route::get('menu/{id}/delete',            ['as' => 'menu.delete',          'uses' => 'MenuController@delete']);
    Route::get('menu/{id}/santosh',            ['as' => 'menu.linkwithpage',          'uses' => 'MenuController@delete']);

    Route::get('page',                        ['as' => 'page.index',           'uses' => 'PageController@index']);
    Route::get('page/add',                    ['as' => 'page.add',             'uses' => 'PageController@add']);
    Route::post('page/store',                 ['as' => 'page.store',           'uses' => 'PageController@store']);
    Route::get('page/{id}/edit',              ['as' => 'page.edit',            'uses' => 'PageController@edit']);
    Route::post('page/{id}/update',           ['as' => 'page.update',          'uses' => 'PageController@update']);
    Route::get('page/{id}/delete',            ['as' => 'page.delete',          'uses' => 'PageController@delete']);

    Route::get('sub-menu',                        ['as' => 'sub-menu.index',           'uses' => 'SubMenuController@index']);
    Route::get('sub-menu/add',                    ['as' => 'sub-menu.add',             'uses' => 'SubMenuController@add']);
    Route::post('sub-menu/store',                 ['as' => 'sub-menu.store',           'uses' => 'SubMenuController@store']);
    Route::get('sub-menu/{id}/edit',              ['as' => 'sub-menu.edit',            'uses' => 'SubMenuController@edit']);
    Route::post('sub-menu/{id}/update',           ['as' => 'sub-menu.update',          'uses' => 'SubMenuController@update']);
    Route::get('sub-menu/{id}/delete',            ['as' => 'sub-menu.delete',          'uses' => 'SubMenuController@delete']);


});