<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title_principle_name"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title_principle_name" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']['image']) && !empty($data['row']['image']))
<div id="image_group">
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="image"> Old Image </label>

        <lable class="col-sm-9">

            <img src="{{ asset('images/principle/'.$data['row']['image']) }}" width="200px" for="image" alt="">
            <a href="#" id="remove_principle_image">
                <i class="icon-remove"></i>
            </a>
        </lable>
    </div>
    <div class="space-4"></div>

    <input type="hidden" id="old_image_val" value="{{ $data['row']['image'] }}" name="oldimg">
</div>
    @endif
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="image_upload"> Image </label>

    <div class="col-sm-9">

        <input type="file" name="image" id="image_upload" value="{{ ViewHelper::getData('image', isset($data['row'])?$data['row']:[]) }}" placeholder="Image" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="message"> Principle's Message </label>

    <div class="col-sm-9">
        <textarea name="message" id="message" placeholder="Principle's Message" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('message', isset($data['row'])?$data['row']:[]) }}</textarea>
        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" checked type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>



<script>
    $(document).ready(function () {
        $('#remove_principle_image').click(function () {
            ajax('image','nth');
        });
        $('#title_principle_name').change(function () {
            ajax('title',$('#title_principle_name').val());
        });
    });


    function ajax($input,$value) {
        $.ajax({
            method : 'post',
            url : '{{ route('admin.principle.image.delete',$data['row']['id']) }}',
            data : {
                _token : '{{ csrf_token() }}',
                type : $input,
                value : $value
            },

            success : function (response) {

                var data = $.parseJSON(response);
                if (data.error){

                }else{
                    $('#image_group').hide(500);
                    $('#old_image_val').val(null);
                }

            },
        });
    }

</script>