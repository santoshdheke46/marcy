<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                title: {
                    required: true,
                    minlength: 1
                },
                image: {
                    type: image,
                }



            },
            messages: {
                title: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 8 characters long"
                },
                image: {
                    type: "Choose image file.",
                    minlength: "Your name must be at least 3 characters long",
                }
            }

        });

    });
</script>
