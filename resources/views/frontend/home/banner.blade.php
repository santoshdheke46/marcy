<style>
    .banner {
        @if(isset($data['banner']->image))
            background: url({{ asset('images/banner/'.$data['banner']->image) }}) no-repeat;
            @else
            background: #555555;
            @endif
        min-height:800px;
        background-size: cover;
    }
</style>
<div class="banner" id="home">
    <div class="container">
        <div class="banner-main wow bounceInDown" data-wow-delay="0.3s">


                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @php($counter = 0)
                        @foreach($data['feature'] as $feature)
                            @php($counter++)
                            <div class="item {{ $counter==1?'active':'' }}">
                                <div>
                                    <h1>{{ $feature->title }}</h1>
                                    <span class="bann-line"> </span>
                                    <p>{!! $feature->description !!}</p>
                                </div>
                            </div>
                            @endforeach
                        ...
                    </div>
                </div>

            <a href="#about" class="scroll"><span class="mover"> </span></a>
        </div>
    </div>
</div>