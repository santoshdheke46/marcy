<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page')->insert([
            'title' => 'Home',
            'slug' => '/',
            'page_link' => 'index',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'About Us',
            'slug' => 'about',
            'page_link' => 'about',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Information',
            'slug' => 'information',
            'page_link' => 'informations',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Club and Eca',
            'slug' => 'club-and-ecas',
            'page_link' => 'club-and-eca',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Download',
            'slug' => 'downloads',
            'page_link' => 'download',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Careers',
            'slug' => 'careers',
            'page_link' => 'careers',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Alumni',
            'slug' => 'alumni',
            'page_link' => 'alumni',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Gallery',
            'slug' => 'gallerys',
            'page_link' => 'gallery',
            'status' => 1,
        ]);
        DB::table('page')->insert([
            'title' => 'Contact Us',
            'slug' => 'contactus',
            'page_link' => 'contact-us',
            'status' => 1,
        ]);
    }
}
