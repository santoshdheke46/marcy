<div class="copy-right">
    <div class="container">
        <div class="copy-rights-main wow zoomIn" data-wow-delay="0.3s">
            <p>© 2016 {{ $profile->title }}. All Rights Reserved | Power by  <a href="http://onlinemultimedia.com.np/" target="_blank">OnlineMultimedia</a> </p>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */

            $().UItoTop({ easingType: 'easeOutQuart' });

        });
    </script>
    <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</div>