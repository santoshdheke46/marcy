<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(!isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="slug"> Slug </label>

        <div class="col-sm-9">
            <input type="text" name="slug" id="slug" value="{{ ViewHelper::getData('slug', isset($data['row'])?$data['row']:[]) }}" placeholder="Slug" class="col-xs-10 col-sm-5">
        </div>
    </div>
    <div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="link"> Link </label>

    <div class="col-sm-9">
        <input type="url" name="link" id="link" value="{{ ViewHelper::getData('link', isset($data['row'])?$data['row']:[]) }}" placeholder="Link" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="dropdown"> Dropdown </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="dropdown" value="1" {{ (isset($data['row']) && $data['row']->parent_menu==!0)?'checked':'' }}{{ empty($data['row'])?'checked':'' }} type="radio" id="select_parent_menu_yes" class="ace">
                <span class="lbl"> Yes</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="dropdown" value="0" {{ (isset($data['row']) && $data['row']->parent_menu==0)?'checked':'' }} type="radio" id="select_parent_menu_no" class="ace">
                <span class="lbl"> No</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group" id="parent_menu_hide_gar" {!! (isset($data['row']) && $data['row']->parent_menu==0)?'style="display: none;"':'' !!}>
    <label class="col-sm-3 control-label no-padding-right" for="parent_menu"> Parent Menu </label>

    <div class="col-sm-9">
        <select name="parent_menu" id="parent_menu" class="col-xs-10 col-sm-5">
            @foreach($data['parent_menu'] as $parent_menu)
                @if(isset($data['row']) && $data['row']->title == $parent_menu->title)
                    @continue
                    @endif
                <option value="{{ $parent_menu->id }}" {!! (isset($data['row']->id) && $data['row']->id==$parent_menu->id)?'selected':'' !!}>{{ $parent_menu->title }}</option>
                @endforeach
        </select>

    </div>
</div>
<div class="space-4"></div>

@if(isset($data['page']))
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="page_id"> Page </label>

    <div class="col-sm-9">
        <select name="page_id" id="page_id" class="col-xs-10 col-sm-5">
            <option value="0">Null</option>
            @foreach($data['page'] as $page_id)
                <option value="{{ $page_id->id }}" {!! (isset($data['row']->id) && $data['row']->page_id==$page_id->id)?'selected':'' !!}>{{ $page_id->title }}</option>
                @endforeach
        </select>

    </div>
</div>
<div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="target"> Target </label>

    <div class="col-sm-9">
        <select name="target" id="target" class="col-xs-10 col-sm-5">
            <option {{ (isset($data['row'])&&$data['row']->target==1)?'selected':'' }} value="1">Yes</option>
            <option {{ (isset($data['row'])&&$data['row']->target==0)?'selected':'' }} value="0">No</option>
        </select>

    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>


<script>

    $(document).ready(function () {

        $('#title').keyup(function () {
            var title = $('#title').val();
            $('#slug').val(title.replace(/\s+/g, '-').toLowerCase());
        });

        $('#select_parent_menu_yes').click(function(){
            $('#parent_menu_hide_gar').show(100);
        });

        $('#select_parent_menu_no').click(function(){
            $('#parent_menu_hide_gar').hide(100);
        });

    });

</script>